use enum_delegate::delegate;

#[delegate]
trait Channel {
    const ID: usize;
}

fn main() {
    unreachable!()
}
