use enum_delegate::delegate;

#[delegate]
trait Channel {
    unreachable!();
}

fn main() {
    unreachable!()
}
